import axios from "axios";
import natural from 'natural';

const tokenizer = new natural.SentenceTokenizer();

export const extract = (term) => {
	return axios.get(`https://en.wikipedia.org/api/rest_v1/page/summary/${term}`,
		{
			headers: {'Accept': 'application/json; charset=utf-8'}
		})
		.then((resp) => {
			if (resp.data.type === 'standard') {
				return tokenizer.tokenize(resp.data.extract)[0];
			}
		})
}

export const random = () => {
	return axios.get(`https://en.wikipedia.org/api/rest_v1/page/random/summary`,
		{
			headers: {'Accept': 'application/json; charset=utf-8'}
		})
		.then((resp) => {
			if (resp.data.type === 'standard') {
				return tokenizer.tokenize(resp.data.extract)[0];
			}
		})
}