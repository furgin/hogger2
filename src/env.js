// this special module allows us to call config() while
// importing to force it to happen before any other module
// is imported
import dotenv from 'dotenv'
dotenv.config()

