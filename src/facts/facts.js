import fs from 'fs';
import logger from '../logger.js';
import {range} from '../random.js';

const allFacts = ["bacon"]

export default class Facts {
	facts = {};
	name;

	constructor() {
		allFacts.forEach(name => {
			this.facts[name] = fs.readFileSync(`./src/facts/${name}.txt`).toString().split("\n");
			logger.info(`loaded ${this.facts[name].length} ${name} facts...`)
		})
	}

	handleMessage(message, reply) {
		Object.keys(this.facts)
			.forEach(key => {
				if (message.indexOf(key) !== -1) {
					reply(this.facts[key][range(0, this.facts[key].length)])
				}
			})
	}

}