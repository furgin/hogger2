import './env.js';
import Settings from './settings.js';
import Facts from './facts/facts.js';
import IRC from 'irc-framework';
import logger from './logger.js';
import pad from 'pad';
import {chat} from "./chat.js";

const bot = new IRC.Client()
chat.register(bot);

const config = {
	host: process.env.IRC_SERVER,
	port: parseInt(process.env.IRC_PORT),
	nick: process.env.IRC_NICK,
	channel: process.env.IRC_CHANNEL
}

logger.info(`connect to ${config.host}:${config.port}...`)
bot.connect(config);

bot.on('connected', function () {
	logger.info('connected');
	let channel = bot.channel(config.channel);
	channel.join();
	channel.updateUsers(() => {
		logger.info(JSON.stringify(channel.users));
	});
});

bot.on('debug', (msg) => logger.debug(msg));
bot.on('message', ({type, message}) => logger.debug(`${type}: ${message}`));
bot.on('nick in use', ({reason}) => logger.info(`nick in use: ${reason}`));
bot.on('nick invalid', ({reason}) => logger.info(`attempted_nick: ${reason}`));
bot.on('nick', ({nick, new_nick}) => logger.info(`${nick} now known as ${new_nick}`));

const commands = {}

let nameExp = new RegExp(`^\s*${config.nick}:\s*`);

const ignores = process.env.IGNORE ? process.env.IGNORE.split(",") : [];

const facts = new Facts();

bot.on('message', (event) => {

	// ignore this sender
	if (ignores.indexOf(event.nick) !== -1) {
		return
	}

	const isPublic = event.target.startsWith("#");
	const hasTag = nameExp.test(event.message);

	// ignore public messages that aren't directed to this bot with <name>:
	if (isPublic && !hasTag) {
		// check them for random facts
		facts.handleMessage(event.message,event.throttledReply)
		return;
	}

	// only handle messages of type 'privmsg' here
	if (event.type !== "privmsg") {
		return;
	}

	logger.info(event.nick + ": " + event.message);

	const msg = event.message.replace(nameExp, "").trim();

	const parts = msg.split(" ");
	if (parts.length === 0) return;
	const command = parts[0].trim();
	if (!!commands[command] && command.length > 0) {
		const args = parts.slice(1);
		bot.emit('command', {...event, command, args});
	} else {
		// respond privately to the sender
		event.privateReply = (message) => {
			bot.say(event.nick, message);
		}
		event.respond = (msg) => {
			if (event.target === bot.user.nick) {
				// direct message, no need for a user tag
				event.reply(msg)
			} else {
				event.reply(`${event.nick}: ${msg}`);
			}
		}
		// not a command pass in chat
		bot.emit('chat', {
			...event,
			message: msg
		});
	}
})

bot.addCommand = (command, command_help, fn) => {
	logger.info("command registered: " + command)
	commands[command] = command_help;
	bot.on('command', (event) => {
		if (event.command === command) {
			logger.debug("command: " + command);
			fn(event);
		}
	})
}

bot.addCommand('help',
	"display help information",
	({reply, args}) => {
		reply("commands:")
		Object
			.entries(commands)
			.forEach((e) => {
				reply(`  ${pad(e[0], 10)} ${e[1]}`)
			})
	})

const settings = new Settings();
settings.register(bot);

function throttler() {

	let messageCounter = 0
		, lastMessageSent = new Date().getTime();

	return function (client, raw_events, parsed) {
		parsed.use((command, event, client, next) => {
			if (command === 'message') {
				event.throttledReply = (message) => {
					let verbosity = settings.get("verbosity");
					let random1 = Math.random();
					if (verbosity < random1) {
						logger.debug(`throttled (verbosity ${verbosity}>${random1}): ${message}`);
						return;
					}

					// update counter
					let elapsedMinutes = Math.floor((new Date().getTime() - lastMessageSent) / 60000);

					if (elapsedMinutes >= 1)
						messageCounter = messageCounter / (2 * elapsedMinutes);

					const chance = 1.0 / Math.pow(2, messageCounter);
					if (Math.random() < chance) {
						messageCounter++;
						lastMessageSent = new Date().getTime();
						event.reply(message);
					} else {
						logger.debug("throttled (counter): " + message);
					}
				}
			}
			next();
		});
	}
}

bot.use(throttler());

