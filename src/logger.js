import winston from 'winston';
import {format} from 'logform';

if(process.env.LOG_LEVEL==="debug") console.log("log level: "+process.env.LOG_LEVEL)

const logger = winston.createLogger({
	level: process.env.LOG_LEVEL,
	transports: [
		new winston.transports.Console()
	],
	format: format.combine(
		format.colorize(),
		format.timestamp(),
		format.align(),
		format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`)
	)
});

export default logger;