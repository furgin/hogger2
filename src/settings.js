export default class Settings {
	settings = {
		verbosity: 0.5
	}

	register(bot) {

		bot.addCommand('set',
			"update settings by key",
			({reply, args}) => {
				const usage = () => reply(`usage: set key value`);
				if (args.length < 2) {
					usage();
				} else {
					const [key, value] = args;
					if (!!this.settings[key]) {
						reply(`set ${key} ${value}`)
						this.settings[key] = value;
					} else {
						usage();
					}
				}
			})

		bot.addCommand('settings',
			"list all setting keys and their values",
			({reply, args}) => {
				const usage = () => reply(`usage: list`);
				if (args.length !== 0) {
					usage();
				} else {
					return Object
						.entries(this.settings)
						.forEach((e) => {
							reply(`${e[0]} = ${e[1]}`)
						})
				}
			})

	}

	get(key) { return this.settings[key]; }

}