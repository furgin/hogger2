import logger from "./logger.js";
import {dockStart} from '@nlpjs/basic';
import natural from 'natural';
import wiki from 'wikipedia';

const language = "EN"
const defaultCategory = 'N';
const defaultCategoryCapitalized = 'NNP';
var tokenizer = new natural.WordTokenizer();
var lexicon = new natural.Lexicon(language, defaultCategory, defaultCategoryCapitalized);
var ruleSet = new natural.RuleSet('EN');
var tagger = new natural.BrillPOSTagger(lexicon, ruleSet);
const sentenceTokenizer = new natural.SentenceTokenizer();

const load = (async () => {
	const dock = await dockStart({use: ['Basic']});
	const nlp = dock.get('nlp');
	nlp.addLanguage('en');
	await nlp.addCorpus('./src/hogger-en.json');
	await nlp.train();
	// await nlp.save();
	// await nlp.load();
	return nlp;
});

class Chat {
	constructor() {
		load()
			.then((nlp) => {
				console.log("loaded")
				this.nlp = nlp;
			})
	}

	register(bot) {
		bot.on('chat', (event) => {
			this.nlp.process("en", event.message)
				.then(result => {
					// console.log(JSON.stringify(result, null, 2));

					logger.debug(`message='${event.message}' intent={'${result.intent}'}`)
					if (result.intent !== 'None') {
						if (result.intent === "ask.about") {
							let sentence = tagger.tag(tokenizer.tokenize(event.message));
							// console.log(sentence);
							const nouns = sentence
								.taggedWords
								.filter(tok=>tok.tag.startsWith("N"))
								.map(tok=>tok.token)

							console.log(nouns);

							wiki.page(nouns.join(" "))
								.then(page=>page.summary())
								.then(summary=>{
									// console.log(summary.extract);
									event.respond(sentenceTokenizer.tokenize(summary.extract)[0]);
									// event.reply(summary.extract)
								})
								.catch(()=>
									event.respond("I don't know."))

						} else {
							if (result.answer) {
								event.respond(result.answer);
							} else {
								console.log(result);
							}
						}
					}
				});
		})
	}
}

export const chat = new Chat();