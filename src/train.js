import {dockStart} from '@nlpjs/basic';

(async () => {
	const dock = await dockStart({ use: ['Basic']});
	const nlp = dock.get('nlp');
	nlp.addLanguage('en');
	await nlp.addCorpus('./src/hogger-en.json');
	await nlp.train();
	await nlp.save();
})();