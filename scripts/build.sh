#!/bin/sh

docker build -t hogger2 .
docker tag hogger2 registry.furgin.org/furgin/hogger2:latest
docker push registry.furgin.org/furgin/hogger2:latest
